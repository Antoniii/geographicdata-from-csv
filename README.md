![](https://gitlab.com/Antoniii/geographicdata-from-csv/-/raw/main/Figure_1.png)


![](https://gitlab.com/Antoniii/geographicdata-from-csv/-/raw/main/Figure_2.png)


![](https://gitlab.com/Antoniii/geographicdata-from-csv/-/raw/main/Figure_3.png)


![](https://gitlab.com/Antoniii/geographicdata-from-csv/-/raw/main/Figure_4.png)


![](https://gitlab.com/Antoniii/geographicdata-from-csv/-/raw/main/Figure_5.png)


![](https://gitlab.com/Antoniii/geographicdata-from-csv/-/raw/main/Figure_6.png)


## Sources

* [Geographic Data with Basemap](https://jakevdp.github.io/PythonDataScienceHandbook/04.13-geographic-data-with-basemap.html)
* [california_cities.csv](https://github.com/reddyprasade/DataSet-for-ML-and-Data-Science/blob/master/DataSets/california_cities.csv)
* [pip install basemap](https://pypi.org/project/basemap/)
* [Azimuthal Equidistant Projection](https://matplotlib.org/basemap/stable/users/aeqd.html)
* [Python Geographic Data Visualization Tool Basemap](https://www.sobyte.net/post/2021-11/basemap/)
* [Basemap Tutorial](https://sharkcoder.com/data-visualization/basemap)
* [How to use Basemap (Python) to plot US with 50 states?](https://pythonhint.com/post/4245323220035979/how-to-use-basemap-python-to-plot-us-with-50-states)
* [Connection map with Python and Basemap](https://python-graph-gallery.com/300-draw-a-connection-line/)
* [Bubble map with Python and Folium](https://python-graph-gallery.com/313-bubble-map-with-folium/)
* [Plotting data on a map (Example Gallery)](https://matplotlib.org/basemap/stable/users/examples.html)
* [Basemap_Example.ipynb](https://github.com/athisha-rk/mediumArticlesRelatedFiles/blob/master/Basemap_Example.ipynb)
* [Построение изолиний на карте мира при помощи Python Basemap](https://www.easycoding.org/2016/12/17/postroenie-izolinij-na-karte-mira-pri-pomoshhi-python-basemap.html)
* [OpenStreetMap](https://www.openstreetmap.org/#map=9/52.3110/5.4699)