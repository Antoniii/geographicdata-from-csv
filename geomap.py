# conda activate GEOGRAPHICMAP

# conda install basemap

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap



plt.figure(figsize=(8, 8))
m = Basemap(projection='ortho', resolution=None, lat_0=60, lon_0=30) # широта и долгота
m.bluemarble(scale=0.5)

x, y = m(-21.939852, 64.147182)
plt.plot(x, y, 'ow', markersize=3)
plt.text(x, y, 'Reykjavík', fontsize=8, color='orange')

x1, y1 = m(49.105579, 55.795366)
plt.plot(x1, y1, 'or', markersize=3)
plt.text(x1, y1, 'Kazan', fontsize=8, color='orange')

x2, y2 = m(121.467864, 31.231745)
plt.plot(x2, y2, 'oy', markersize=3)
plt.text(x2, y2, 'Shànghǎi', fontsize=8, color='orange')

x3, y3 = m(46.735549, 24.665633)
plt.plot(x3, y3, 'og', markersize=3)
plt.text(x3, y3, 'ar-Riyāḍ', fontsize=8, color='black')

x4, y4 = m(-74.004105, 40.715881)
plt.plot(x4, y4, 'or', markersize=3)
plt.text(x4, y4, 'New York City', fontsize=8, color='orange')
# plt.show()



fig = plt.figure(figsize=(8, 8))
m = Basemap(projection='lcc', resolution=None,
            width=6E6, height=6E6, 
            lat_0=50., lon_0=0.0,)
m.etopo(scale=0.5, alpha=0.5)

# Map (long, lat) to (x, y) for plotting
x, y = m(-0.12574, 51.5085)
plt.plot(x, y, 'ok', markersize=5)
plt.text(x, y, 'London', fontsize=12)

x1, y1 = m(2.350974, 48.856551)
plt.plot(x1, y1, 'ob', markersize=5)
plt.text(x1, y1, 'Paris', fontsize=12)

# plt.show()



from itertools import chain

def draw_map(m, scale=0.2):
    # draw a shaded-relief image
    m.shadedrelief(scale=scale)
    
    # lats and longs are returned as a dictionary
    lats = m.drawparallels(np.linspace(-90, 90, 13))
    lons = m.drawmeridians(np.linspace(-180, 180, 13))

    # keys contain the plt.Line2D instances
    lat_lines = chain(*(tup[1][0] for tup in lats.items()))
    lon_lines = chain(*(tup[1][0] for tup in lons.items()))
    all_lines = chain(lat_lines, lon_lines)
    
    # cycle through these lines and set the desired style
    for line in all_lines:
        line.set(linestyle='-', alpha=0.3, color='w')

fig = plt.figure(figsize=(8, 8))
m = Basemap(projection='ortho', resolution=None,
            lat_0=50, lon_0=0)

draw_map(m)

x, y = m(30.313921, 59.938885)
plt.plot(x, y, 'ob', markersize=5)
plt.text(x+1, y+5, 'St. Petersburg', fontsize=6)

x1, y1 = m(4.891469, 52.373194)
plt.plot(x1, y1, 'or', markersize=5)
plt.text(x1+1, y1+5, 'Amsterdam', fontsize=6)

# plt.show()



fig = plt.figure(figsize=(8, 8))
m = Basemap(projection='lcc', resolution=None,
            lon_0=0, lat_0=50, lat_1=45, lat_2=55,
            width=1.6E7, height=1.2E7)
draw_map(m)
x, y = m(30.313921, 59.938885)
plt.plot(x, y, 'ob', markersize=5)
plt.text(x+1, y+5, 'St. Petersburg', fontsize=6)
# plt.show()


fig = plt.figure(figsize=(8, 6), edgecolor='w')
m = Basemap(projection='moll', resolution=None,
            lat_0=0, lon_0=0)
draw_map(m)
x, y = m(30.313921, 59.938885)
plt.plot(x, y, 'or', markersize=5)
plt.text(x+10, y+10, 'St. Petersburg', fontsize=6, color='Black')
plt.show()
